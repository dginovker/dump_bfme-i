==================================================================
Informations l�gales
==================================================================

CE LOGICIEL EST FOURNI TEL QUEL SANS GARANTIE D'AUCUNE SORTE,
QU'ELLE SOIT EXPRESSE OU IMPLICITE, CECI INCLUT LES GARANTIES
COMMERCIALES ET FORMELLES POUR UN USAGE PARTICULIER LIMITEES DANS
LE PRESENT DOCUMENT. ELECTRONIC ARTS NE PEUT ETRE TENU RESPONSABLE
DES DEGATS SPECIAUX, ACCIDENTELS OU CONSEQUENTS A LA POSSESSION, 
L'UTILISATION OU LE DYSFONCTIONNEMENT DE CE LOGICIEL ELECTRONIC
ARTS.

CERTAINS PAYS NE LIMITENT PAS LES GARANTIES DANS LA DUREE
ET/OU LES EXCLUSIONS OU LES LIMITATIONS DES DEGATS ACCIDENTELS OU
CONSEQUENTS. LA LIMITATION CI-DESSUS PEUT NE PAS VOUS CONCERNER.
CETTE GARANTIE VOUS OCTROIE DES DROITS SPECIFIQUES ET VOUS POUVEZ
AVOIR D'AUTRES DROITS SELON LES TERRITOIRES.

ELECTRONIC ARTS SE RESERVE LE DROIT DE MODIFIER CE FICHIER ET LE
LOGICIEL A TOUT MOMENT SANS PREAVIS.
CE FICHIER ET LE LOGICIEL ASSOCIE SONT SOUMIS AUX DROITS 
D'AUTEURS. TOUS LES DROITS SONT RESERVES. AUCUNE PARTIE DE CE
FICHIER OU DU LOGICIEL ASSOCIE NE PEUT ETRE COPIEE, REPRODUITE,
TRADUITE OU TRANSFORMEE SANS ACCORD PREALABLE ECRIT DE 
ELECTRONIC ARTS.


==================================================================
Copyright Electronic Arts
==================================================================

Logiciel et documentation � 2004 Electronic Arts Inc.

TOUS DROITS RESERVES.


==================================================================
Le Seigneur des Anneaux(tm)- La Bataille pour la Terre du Milieu(tm)
Version 1.0 Fichier Lisez-moi

11 Novembre 2004
==================================================================

Merci d'avoir achet� Le Seigneur des Anneaux(tm)- La Bataille pour
la Terre du Milieu(tm). Ce fichier contient des informations qui 
vous aideront � bien d�buter votre voyage � travers la Terre du
Milieu.


==================================================================
Configuration requise
==================================================================

DirectX 9.0b est requis pour jouer � La Bataille pour la Terre du 
Milieu(tm). Pour installer DirectX 9.0b ou sup�rieur, rendez-vous
sur :

  http://www.microsoft.com/windows/directx/default.aspx (site en
anglais)

CONFIGURATION REQUISE :

  - Windows XP ou Windows 2000
  - Processeur Intel Pentium IV ou AMD Athlon � 1,3 GHz
  - 256 Mo de RAM
  - Carte graphique �quivalente � Nvidia GeForce 2 avec 32 Mo de RAM
  - Carte son compatible DirectX 9.0b	
  - 4�Go d'espace libre sur le disque dur

Veuillez noter que m�me si vous pourrez jouer � La Bataille pour la
Terre du Milieu(tm) avec la configuration ci-dessus, la jouabilit�
sera am�lior�e si votre syst�me dispose de plus de 256�Mo de RAM.

Les cartes Nvidia GeForce 2 MX ne sont pas compatibles. 

==================================================================
Proc�dure d'installation
==================================================================

Avant de lancer La Bataille pour la Terre du Milieu(tm), 
assurez-vous d'avoir install� les derniers pilotes pour votre 
carte graphique et votre carte son. Vous pouvez t�l�charger les 
pilotes appropri�s sur le site Internet du fabricant de la carte.

Il est recommand� de fermer tous les autres programmes, car ils 
peuvent entrer en conflit avec le jeu. Ceci inclut tous les 
anti-virus actifs pouvant alt�rer les performances.

Pour installer La Bataille pour la Terre du Milieu(tm) :

  1) Ins�rez le DVD de La Bataille pour la
     Terre du Milieu(tm) dans le lecteur DVD-ROM.

     Si l'installation ne s'ex�cute pas automatiquement lorsque le 
     DVD est ins�r�, cela signifie que cette option est d�sactiv�e.
     Pour activer l'ex�cution automatique, veuillez consulter le 
     manuel de Windows.

     Si vous n'arrivez pas ou ne d�sirez pas activer l'ex�cution
     automatique, utilisez l'explorateur Windows pour parcourir le 
     DVD-ROM de La Bataille pour la Terre du Milieu(tm) et
     double-cliquez sur l'application "Setup" dans le r�pertoire
     racine.

  2) Suivez les instructions qui s'affichent � l'�cran pour
     installer La Bataille pour la Terre du Milieu(tm).

  3) Vous devez enregistrer La Bataille pour la Terre du Milieu(tm)
     lorsque le programme d'installation vous le demande pour 
     pouvoir y jouer en ligne. Vous pouvez enregistrer le jeu 
     ult�rieurement en cliquant sur le bouton "S�inscrire"
     dans le menu en ligne du jeu.

Si vous rencontrez des probl�mes avec le num�ro de s�rie fourni
avec La Bataille pour la Terre du Milieu(tm), ou si vous avez besoin 
de toute autre assistance technique, veuillez cliquer sur "le bouton
"Assistance technique" de l'ex�cution automatique. Vous pouvez 
�galement consulter le site officiel du produit et s�lectionnez 
l'option "Assistance". Vous devez enregistrer le produit pour 
obtenir une assistante compl�te.

http://www.eagames.com/official/lordoftherings/thebattleformiddleearth/fr/home.jsp


==================================================================
D�sinstaller La Bataille pour la Terre du Milieu(tm)
==================================================================


  Pour d�sinstaller La Bataille pour la Terre du Milieu(tm) :

  1) Ouvrez le menu d�marrer.
  2) Choisissez "Param�tres".
  3) Choisissez "Panneau de configuration".
  4) Choisissez "Ajout/suppression de programmes".
  5) S�lectionnez "La Bataille pour la Terre du Milieu(tm)" et 
     cliquez sur le bouton "Modifier/Supprimer" situ� � c�t� du
     nom du jeu.
  6) Suivez les instructions � l'�cran pour proc�der � la
     d�sinstallation.


==================================================================
Windows 2000
==================================================================

Si vous essayez de lancer La Bataille pour la Terre du Milieu(tm) 
sous Windows 2000, assurez-vous d'avoir t�l�charg� et install� 
depuis le site de Microsoft le Service Pack 4 ainsi que toutes les
mises � jour critiques.


==================================================================
D�pannage
==================================================================

Un pilote graphique ou son qui n'est pas � jour peut avoir des 
cons�quences sur la jouabilit� ou ralentir le jeu. Cela peut m�me,
dans certains cas, emp�cher le lancement du jeu.
Pour profiter au maximum de La Bataille pour La Terre du Milieu(tm),
assurez-vous d'avoir install� les derniers pilotes de la carte
graphique et de la carte son. Ces pilotes sont g�n�ralement
disponibles sur le site du fabricant du mat�riel.
Si vous n'�tes pas s�r du mod�le de votre carte graphique ou son
ou si vous avez besoin d'aide pour mettre � jour les pilotes,
veuillez consulter la documentation livr�e avec le syst�me ou le 
p�riph�rique.


==================================================================
Sauvegardes et param�tres
==================================================================

Les fichiers de sauvegarde de La Bataille pour la Terre du Milieu(tm) 
et les autres param�tres utilisateur sont enregistr�s dans le 
r�pertoire "La Bataille pour la Terre du Milieu", 
g�n�ralement accessible de la mani�re suivante : 
parcourez le lecteur C:\, ouvrez le r�pertoire "Documents and Settings",
ouvrez le r�pertoire de votre nom sous Windows et ouvrez le 
r�pertoire "Application Data". Si vous ne voyez pas de r�pertoire 
"Application Data", lisez les instructions suivantes :

  1) Ouvrez l'explorateur Windows, en appuyant sur la touche
     Windows du clavier tout en appuyant sur la touche "E".
  2) Cliquez sur "Outils" dans le menu du haut.
  3) Choisissez "Options des dossiers".
  4) Choisissez l'onglet "Affichage".
  5) Cliquez sur le bouton en face de "Afficher les fichiers et 
     dossiers cach�s" dans la cat�gorie "Param�tres avanc�s".

  6) Cliquez sur "Appliquer" puis sur "OK".
  7) Le r�pertoire "Application Data" devrait maintenant 
     appara�tre.

Attention, il n'y a qu'un seul fichier de sauvegarde automatique
pour La Bataille pour la Terre du Milieu(tm). Si vous commencez une
des campagnes puis passez � la suivante, la sauvegarde
automatique de la premi�re campagne sera �cras�e.


==================================================================
Param�tres de ligne de commande
==================================================================

La Bataille pour la Terre du Milieu(tm) est compatible avec
plusieurs param�tres de ligne de commande qui vous donneront
davantage de flexibilit� dans votre mani�re d'ex�cuter le jeu. Pour
d�finir un param�tre de ligne de commande, effectuez les op�rations
suivantes�:

  1) Allez dans le r�pertoire du fichier ex�cutable du jeu. On le
     trouve habituellement dans C:\Program Files\EA Games\
     La Bataille pour la Terre du Milieu(tm)\lotrbfme.exe.
  2) Faites un clic droit sur l'ic�ne lotrbfme.exe et s�lectionnez
     "Cr�er un raccourci".
  3) Un raccourci du fichier ex�cutable sera cr�� dans le m�me
     r�pertoire. Il sera g�n�ralement nomm� "Raccourci vers
     lotrbfme.exe".
  4) Faites un clic droit sur ce nouveau raccourci et s�lectionnez
     "Propri�t�s".
  5) A la fin du chemin dans le champ "Cible�:", ajoutez un espace
     suivi d'un "-" et du param�tre de ligne de commande.

Les param�tres de ligne de commande suivants sont compatibles avec
La Bataille pour la Terre du Milieu(tm)�:

     -noshellmap (lance le jeu sans l'arri�re plan tournant de
                  de Barad-d�r.)
     -mod (permet aux moddeurs de charger des �l�ments personnalis�s
           dans le jeu. Ajoutez simplement la ligne de commande "-mod"
           � la fin du chemin cible du fichier lotrbfme.exe, suivie du
           nom d'un fichier .big ou d'un r�pertoire cr�� par
           l'utilisateur. Un chemin correct devrait ressembler � �a�:
           "C:\Program Files\EA Games\La Bataille pour la Terre du
           Milieu(tm)\lotrbfme.exe" -mod MonLOTRMod.big. Tous les
           �l�ments cr��s par l'utilisateur doivent �tre plac�s dans
           le r�pertoire "La Bataille pour la Terre du Milieu(tm)".)
     -noaudio (lance le jeu sans aucun fichier audio.)
      -xres (oblige le jeu � tourner avec une r�solution horizontale
            sp�cifique.)
     -yres (oblige le jeu � tourner avec une r�solution verticale
            sp�cifique.)

==================================================================
Probl�mes connus
==================================================================

Sur certaines cartes graphiques param�tr�es pour deux �crans, des
d�formations visuelles pourraient appara�tre � l'�cran. Si vous
rencontrez ce probl�me, allez dans le menu principal du jeu et 
cliquez sur "Options". Choisissez de nouvelles r�solutions dans 
"Options affichage" puis cliquez sur "Accepter changements".
Cela devrait r�gler le probl�me. Si vous le souhaitez, vous pourrez
revenir � la r�solution de d�part.

Pour les cartes ATI Radeon 7500, de l�gers effets multicolores
peuvent appara�tre en cours de partie. Ce probl�me est peu fr�quent
et ne devrait en aucun cas g�ner la jouabilit�. Si vous rencontrez
ce probl�me, veuillez consulter le site Internet de ATI pour v�rifier
que vous avez bien install� les derni�res pilotes en date.

Sur certaines cartes son Creative Labs Audigy, des probl�mes sonores
peuvent survenir lorsque l'option EAX3 est s�lectionn�e dans le menu
Options. V�rifiez que vous avez install� les derniers pilotes Audigy.

Si vous choisissez d'utiliser l'option EAX3, assurez-vous que le type
de haut-parleurs appropri� est s�lectionn� dans Windows, de la fa�on
suivante :

  1) Dans Windows, cliquez sur "D�marrer".
  2) Choisissez "Param�tres".
  3) Choisissez "Panneau de configuration".
  4) S�lectionnez "Sons et p�riph�riques audio".
  5) S�lectionnez l'onglet "Volume".
  6) Dans la section "Param�tres des haut-parleurs" cliquez sur
     "Param�tres avanc�s...".
  7) S�lectionnez "Haut-parleurs".
  8) Dans la section "Configuration des haut-parleurs", affichez
     le menu et choisissez le haut-parleur appropri�.
  9) Cliquez sur "Appliquer" puis sur "OK".

Si un �conomiseur d'�cran est activ� et se met en route pendant le
chargement d'une mission, la fen�tre de jeu sera r�duite dans la
barre des t�ches de Windows, situ�e en r�gle g�n�rale tout en bas de
l'�cran. Si cela se produit, d�sactivez l'�conomiseur d'�cran et
agrandissez la fen�tre en cliquant sur le bouton "Le Seigneur des
Anneaux(tm) - La Bataille pour la Terre du Milieu(tm)" dans la
barre des t�ches.

Les utilisateurs de Windows 2000 peuvent observer que, apr�s avoir
quitt� une partie en mode escarmouche avec au moins six adversaires
contr�l�s par l'IA, la transition vers le menu principal se fait 
lentement. Si vous observez un tel ralentissement, essayez de changer
le param�tres "D�tails" sur Faibles dans le menu Options du jeu.

==================================================================
Editeur de cartes (WorldbuilderI)
==================================================================
Le logiciel est en anglais et livr� en l��tat. EA n�accorde aucun
support sur ce logiciel.

Apr�s installation, l'�diteur se trouve dans le r�pertoire
suivant :
C:\Program Files\EA GAMES\La Bataille pour la Terre du Milieu(tm)
\WorldbuilderI


==================================================================
Informations multijoueur
==================================================================

Lorsque vous jouez en ligne � La Bataille pour la Terre du Milieu(tm),
votre "pseudo" doit comporter au minimum quatre caract�res. Si vous 
tentez de vous connecter en utilisant un nom � trois caract�res ou
moins, le bouton "Se connecter" restera inactif. De m�me, les noms 
sont limit�s � 15 caract�res maximum et ne doivent pas
commencer par des caract�res non alphab�tiques.

Les statistiques multijoueur sont sauvegard�es automatiquement et
envoy�es aux classements 1 contre 1 ou 2 contre 2 uniquement lorsque
vous jouez � des parties rapides. La partie rapide trouve 
automatiquement un joueur ayant sensiblement le m�me rang que vous 
ainsi qu'une connexion appropri�e. Vous pouvez cliquer sur le bouton 
"Elargir recherche" pour masquer les limitations de rang et de 
connexion.

==================================================================
NAT/Pare-feu
==================================================================

Si vous jouez � La Bataille pour la Terre du Milieu(tm) derri�re un
Routeur USRobotics, vous devez mettre � jour le logiciel � la version
2.7 ou sup�rieure. Consultez l'assistance sur le site de USRobotics 
pour obtenir des d�tails sur la proc�dure � suivre.

Si vous jouez derri�re un routeur D-Link, cochez la case "D�lai envoi"
dans le menu d'options multijoueur de La Bataille pour La Terre du
Milieu(tm). Pour y acc�der depuis le menu principal, cliquez sur le
bouton Multijoueur, puis sur le bouton Internet, connectez-vous et
cliquez enfin sur Options.
Par ailleurs, si vous jouez derri�re un D-Link DI-704, vous devrez mettre
� jour le logiciel � la version 2.75 build 3 ou sup�rieure. Consultez
l'assistance sur le site D-Link pour plus de d�tails sur la proc�dure
� suivre.

Il est important de noter que les routeurs D-Link ont des probl�mes non
r�solubles lors de tentatives de connexion � un routeur USRobotics et
inversement. Ces routeurs n'ont aucun probl�me � �tablir de connexion
avec des routeurs d'autres marques, mais la connexion risque d'�chouer
lorsqu'ils tentent une connexion l'un vers l'autre.

Si vous jouez derri�re un D-Link DI-604, vous devrez ajouter deux
lignes au fichier d'options .ini. Ce fichier .ini est situ� dans votre
r�pertoire "La Bataille pour la Terre du Milieu(tm)". Pour acc�der � ce
r�pertoire, veuillez consulter la partie Sauvegardes et param�tres
ci-dessus. Pour modifier le fichier d'options .ini, faites un clic droit
sur celui-ci et choisissez "Modifier". En bas du fichier, ajoutez les
lignes suivantes�:

   FirewallBehavior = 19
   FirewallPortAllocationDelta = 2

Lorsque vous avez ajout� ces lignes, fermez le fichier d'options .ini.
Windows vous demandera si vous souhaitez ou non sauvegarder. Choisissez
"Oui". Si vous mettez � jour votre fichier d'options .ini, vous n'avez
pas besoin de cliquer sur "Actualiser NAT". Pour des informations plus
d�taill�es sur l'actualisation du NAT, reportez-vous � la fin de la
partie NAT/pare-feu de ce LisezMoi.

Veuillez noter que si plusieurs joueurs partagent une seule connexion
D-Link DI-604, il est possible que vous ayez des probl�mes de connexion.

Il est important de noter que les routeurs Belkin et USRobotics
pr�sentent des probl�mes non r�solubles, mais seulement lorsqu'ils
essaient de se connecter l'un � l'autre pendant des parties rapides.
De plus, les routeurs Belkin et USRobotics connaissent des difficult�s
pour se connecter aux routeurs D-Link DI-604 pendant des parties rapides.

Si vous jouez � La Bataille pour la Terre du Milieu(tm) derri�re un 
pare-feu logiciel, il faut ajouter le fichier ex�cutable du jeu � la
liste des programmes autoris�s fournie par le logiciel et sp�cifier
le chemin exact du fichier ex�cutable, g�n�ralement situ� dans
"C:\Program Files\EA Games\La Bataille pour la Terre du Milieu(tm)\
lotrbfme.exe".
Il faut �galement ajouter � cette liste le fichier "game.dat", situ�
dans le m�me r�pertoire.

Vous ne devriez pas avoir besoin d'utiliser les options de suivi des
ports/d�clenchement des ports pour jouer derri�re un pare-feu.
La Bataille pour la Terre du Milieu(tm) devrait fonctionner 
correctement avec la plupart des pare-feu personnels.

Si les propri�t�s du routeur ont chang� depuis la derni�re fois que
vous avez jou� � La Bataille pour la Terre du Milieu(tm), cliquez sur
le bouton "Actualiser NAT" dans les options multijoueur du jeu. Pour y
acc�der depuis le menu principal, cliquez sur le bouton Multijoueur,
puis sur le bouton Internet, connectez-vous et cliquez enfin sur
Options.

La Bataille pour la Terre du Milieu(tm) utilise les ports UDP 8088-
28088.

Si vous rencontrez des probl�mes divers de connexion via NAT, quittez
le jeu, effacez le fichier "options.ini" dans votre r�pertoire "La
Bataille pour la Terre du Milieu(tm)", relancez le jeu et cliquez
sur le bouton "Actualiser NAT" dans les options multijoueur du jeu.

De plus, si vous �tes �quip� d'un modem de c�ble et d'un routeur,
contactez votre fournisseur d'acc�s � Internet/au c�ble pour 
d�sactiver le pare-feu int�gr� du modem.


==================================================================
*Privil�ges administrateur
==================================================================

Vous ne pourrez pas ajouter La Bataille pour la Terre du Milieu(tm)
� la liste des programmes autoris�s du pare-feu si vous n'�tes pas
connect� � Windows en tant qu'administrateur. Vous devrez vous
connecter en tant qu'administrateur ou ajouter votre compte � la liste
d'administration, selon les instructions suivantes :

  1) Fermez votre session de Windows et connectez-vous avec le compte
     administrateur.
  1) Une fois connect�, cliquez sur "D�marrer" dans la barre des
     t�ches.
  2) Choisissez "Param�tres".
  3) Choisissez "Panneau de configuration".
  4) Choisissez "Comptes d'utilisateurs".
  5) S�lectionnez l'onglet "Utilisateurs".
  6) Dans la section "Utilisateurs de cet ordinateur :" cliquez sur le
     nom de votre compte.
  7) Cliquez sur "Propri�t�s".
  8) S�lectionnez l'onglet "Appartenance au groupe".
  9) Cliquez sur le bouton "Autre :"
  10) Dans le menu d�roulant, s�lectionnez "Administrateurs".
  11) Cliquez sur "Appliquer" puis sur "OK".
